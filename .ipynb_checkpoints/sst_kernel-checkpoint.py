import torch
import numpy as np
import pandas as pd
import csv, random
from matplotlib import pyplot as plt
import seaborn as sbn
import torch.optim as optim
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset,DataLoader
from torch.utils.data import random_split
from gensim.models import KeyedVectors
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.sampler import SequentialSampler
import string

sbn.set()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

class Net(torch.nn.Module):
    def __init__(self,in_c,out_c,kernel):
        super(Net,self).__init__()
#         self.conv1=nn.Conv2d(in_c,out_c,(2,kernel+2),padding=1)
        self.conv2=nn.Conv2d(in_c,out_c,(3,kernel+4),padding=2)
        self.conv3=nn.Conv2d(in_c,out_c,(4,kernel+6),padding=3)
        self.conv4=nn.Conv2d(in_c,out_c,(5,kernel+8),padding=4)
        self.fc1=torch.nn.Linear(out_c*3,5)
    def forward(self,x):
#         x1=F.relu(self.conv1(x))
#         x1=F.max_pool2d(x1,(x1.shape[2], 1), 1)
        x2=F.relu(self.conv2(x))
        x2=F.max_pool2d(x2,(x2.shape[2], 1), 1)
        x3=F.relu(self.conv3(x))
        x3=F.max_pool2d(x3,(x3.shape[2], 1), 1)
        x4=F.relu(self.conv3(x))
        x4=F.max_pool2d(x4,(x4.shape[2], 1), 1)
#         cat1=torch.cat((x1,x2),1)
        cat2=torch.cat((x3,x4),1)
#         cat2=torch.cat((cat1,cat2),1)#   
        cat2=torch.cat((x2,cat2),1)
        flat=cat2.view(cat2.shape[0],-1)
        out=self.fc1(F.dropout(flat,p=.5))
        return out

def data_stats(df):    
    word_len=[]
    sen_count=[]
    word_count=[]
    for i in range(len(df)):
        pp1=df.loc[i][0].split(" ")
        word_count.append(len(pp1))

    # plt.subplot(211)
    p_value=90
    plt.title("word_per_sentence plot, total sentence="+str(len(word_count)))
    plt.plot(word_count)
    plt.figtext(.35,.75,str(p_value)+"_percentile ="+str(np.percentile(word_count,p_value)))
    plt.show()
#     plt.title("sentence_per_review plot, total Review="+str(len(sen_count)))
#     # plt.subplot(221)
#     plt.plot(sen_count)
#     plt.title("sentence_per_review plot, total Review="+str(len(sen_count)))
#     plt.figtext(.55,.75, str(p_value)+"percentile ="+str(np.percentile(sen_count,p_value)))
#     plt.plot(sen_count)
#     plt.show()


class imdb_dataset(Dataset):
    def __init__(self,path,transform,h,w,embad_model):
        self.data_frame=pd.read_csv(path,header=None)
        self.transform=transform
        self.model=embad_model
        self.h=h
        self.w=w
        self.data_dict={}
    
    def __len__(self):
        return len(self.data_frame)
    
    def __getitem__(self, idx):
        return {'review':self.data_dict[idx]['review'],'rating':self.data_dict[idx]['rating']}
    
    def build_dict(self):
        for i in range(len(self.data_frame)):
            sample=self.data_frame.loc[i]
            mat,label=self.transform(sample,self.model,self.h,self.w)
            self.data_dict[i]={'review':mat,'rating':label}
    

def sen_to_mat(r,model,h,w):
    X=torch.randn((h,w))
    label=0
    table = str.maketrans('', '', string.punctuation)
    for j,w in enumerate(r[0].split(' ')):
        w1=w.translate(table)
        if w1 in model.vocab:
            X[j,:]=torch.Tensor(model[w1]).view(1,-1)
        label=r[1]
        if(j>=h-1):
            break
    return X,label

def plot_fig(titel,xlabel,ylabel,x,y):
    plt.figure(figsize=(10,5))
    plt.title(title)
    plt.xlabel(xlabel)
    plt.xticks(x)
    plt.ylabel(ylabel)
    plt.plot(x,y)
    plt.savefig(title)
    plt.show()

# from gensim.scripts.glove2word2vec import glove2word2vec
# glove_input_file = 'glove/glove.6B.50d.txt'
# word2vec_output_file = 'glove.6B.50d.txt.word2vec'
# glove2word2vec(glove_input_file, word2vec_output_file)
# aaaa?
if __name__=='__main__':
    print('main')
    filename='glove.6B.50d.txt.word2vec'
    model=KeyedVectors.load_word2vec_format(filename,binary=False)

    table = str.maketrans('', '', string.punctuation)
    # stripped = [w.translate(table) for w in words]

    cnn_model=Net(1,50,50)
    if torch.cuda.device_count() > 1:
        cnn_model=nn.DataParallel(cnn_model)
    cnn_model.to(device)

    device = torch.device("cuda:0")
    current=0
    mb_size=500

    channel=1
    height=50
    width=50

    loss_ls=[]
    valloss_ls=[]
    train_acc=0
    crt=nn.CrossEntropyLoss()
    criterion=crt.to(device)


    optimizer = optim.Adam(cnn_model.parameters(),lr=.1,weight_decay=.0425)

    imdb=imdb_dataset('data1/train.csv',sen_to_mat,height,width,model)
    imdb.build_dict()
    imdbt=imdb_dataset('data1/test.csv',sen_to_mat,height,width,model)
    imdbt.build_dict()
    imdbv=imdb_dataset('data1/valid.csv',sen_to_mat,height,width,model)
    imdbv.build_dict()
    total_doc=len(imdb)
    total_vdoc=len(imdbv)
    total_tdoc=len(imdbt)
    indices=list(range(0,total_doc))
    np.random.shuffle(indices)

    train_sampler = SubsetRandomSampler(indices[0:int(total_doc)])
    # train_sampler = SequentialSampler(indices[0:200000])
    val_sampler = SubsetRandomSampler(range(0,total_vdoc))
    # val_sampler = SequentialSampler(indices[20000:250000])
    test_sampler = SubsetRandomSampler(range(0,total_tdoc))
    train_loader=DataLoader(imdb,batch_size=mb_size,num_workers=8,sampler=train_sampler)

    valid_loder=DataLoader(imdbv,batch_size=500,num_workers=8,sampler=val_sampler)
    test_loder=DataLoader(imdbt,batch_size=500,num_workers=8,sampler=val_sampler)

    filters=[]
    filters=[1,2,5]
    for i in range(10,151,10):
        filters.append(i)

    train_accl=[]
    test_accl=[]
    loss_ls=[]
    valloss_ls=[]
    wd=.0425
    print('Training Start..')
    for f in filters:
        cnn_model=Net(1,f,50)
        if torch.cuda.device_count() > 1:
            cnn_model=nn.DataParallel(cnn_model)
        cnn_model.to(device)
        lr=0.01
        optimizer = optim.Adam(cnn_model.parameters(),lr=lr,weight_decay=wd)
        itrn=0
        num_itrn=150
        tl=[10000,9999,9998]
        tlac=[]
        while(itrn<num_itrn):
            print('itrn '+str(itrn),' LR '+str(lr)+' filters '+str(f))
            train_acc=0
            total_loss=0;
            cnn_model.train()
            for (num_batch,mb_dict) in enumerate(train_loader):
                mb=mb_dict['review'].to(device,dtype=torch.float)
                mb.unsqueeze_(1)
                lbl=mb_dict['rating'].to(device,dtype=torch.long)
                optimizer.zero_grad()
                ypred=cnn_model(mb)
        #         loss=-torch.dot(lbl,torch.log(ypred.reshape(lbl.shape[0])))-torch.dot((1-lbl),torch.log((1-ypred).reshape(lbl.shape[0])))
                loss=criterion(ypred,lbl)
                loss.backward()
                optimizer.step()
                total_loss=total_loss+loss.item()
                ypred=F.softmax(ypred,dim=1)
                ypred=torch.argmax(ypred,dim=1)
                temp=torch.sum(ypred.data.reshape(lbl.shape)==lbl.data)
                train_acc+=temp.item()
            tl.append(total_loss)
            tlac.append(train_acc/total_doc)
            if(tl[-2]<tl[-1] and tl[-3]<tl[-2]):
                lr=lr/2
                if(lr<.00005):
                    break
                optimizer = optim.Adam(cnn_model.parameters(),lr=lr,weight_decay=wd)
#             if(itrn>=10 and itrn%10 ):
#                 if(abs(tlac[-10]-tlac[-1]) < .1):
#                     break;
            itrn+=1
        #         if(num_batch*mb_size%5==0):
        #             print(loss.item())
        #             itrn_loss.append(loss.item())
        #         if(itrn>15):
        #             torch.save(cnn_model.state_dict(),"saved_model/"+"itrn"+str(itrn-1)+"tacc"+str(int(train_acc*1000))+"vacc"+str(int(val_acc*1000)))
        #             print("train_acc",train_acc)
        cnn_model.eval()
        with torch.no_grad():
            val_acc=0
            for (num_val,val_dict) in enumerate(test_loder):
                tb=val_dict['review'].to(device,dtype=torch.float)
                tb.unsqueeze_(1)    
                tlbl=val_dict['rating'].to(device,dtype=torch.long)
                typred=cnn_model(tb)
                val_loss=criterion(typred,tlbl)
    #             val_loss=-torch.dot(tlbl,torch.log2(typred.reshape(tlbl.shape[0])))-torch.dot((1-tlbl),torch.log2((1-typred).reshape(tlbl.shape[0])))
        #       val_loss=criterion(typred.reshape(tlbl.shape[0]),tlbl)
                #loss=criterion(ypred,lbl)
                typred=F.softmax(typred,dim=1)
                typred=torch.argmax(typred,dim=1)
                temp1=torch.sum(typred.data.reshape(tlbl.shape)==tlbl.data)
                val_acc+=temp1.item()


    #         print("Training loss ",total_loss)
    #         print("Training Accuracy",float(train_acc)/int(total_doc))
    #         print("Validation loss ",val_loss)
    #         print('Validation Accuracy',float(val_acc)/int(total_vdoc))
            loss_ls.append(total_loss)
            valloss_ls.append(val_loss.item())
            train_accl.append(float(train_acc)/int(total_doc))
            test_accl.append(float(val_acc)/int(total_vdoc))
            itrn=itrn+1
            
    folder="output/"
    title="Number of filters Vs Train Loss for different kernel"
    xlabel="Numer of filters"
    ylabel="Cross Entorpy Loss"
    x=filters
    y=loss_ls
    plot_fig(folder+title,xlabel,ylabel,x,y)

    title="Number of filters Vs Test Loss for different kernel"
    xlabel="Numer of filters"
    ylabel="Cross Entorpy Loss"
    x=filters
    y=valloss_ls
    plot_fig(folder+title,xlabel,ylabel,x,y)

    title="Number of filters Vs Train Accuracy for different kernel"
    xlabel="Numer of filters"
    ylabel="Train Accuracy"
    x=filters
    y=train_accl
    plot_fig(folder+title,xlabel,ylabel,x,y)

    title="Number of filters Vs Test Accuracy for different kernel"
    xlabel="Numer of filters"
    ylabel="Test accuracy"
    x=filters
    y=test_accl
    plot_fig(folder+title,xlabel,ylabel,x,y)